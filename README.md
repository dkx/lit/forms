# @dkx/lit-forms

Lit forms inspired by angular reactive forms.

* Validation
* Dynamic fields (arrays)
* Automatic component updates
* Material components

## Installation

```bash
$ npm install --save @dkx/lit-forms
```

**Dependencies:**

* lit >=2.0.0-rc.1
* rxjs >=6.0.0

## Example

**Define form:**

```typescript
this.form = defaultFormBuilder.create(this, root => [
    root.control('email', ''),
    root.control('password', ''),
]);
```

**Render form:**

```html
<form @submit="${this.handleFormSubmit}">
    <div>
        <label for="email">Email</label>
        <input ${this.form.attach('email')} type="email" required>
    </div>
    <div>
        <label for="password">Password</label>
        <input ${this.form.attach('password')} type="password" required>
    </div>
    <input type="submit">Sign in</input>
</form>
```

**Handle submit:**

```typescript
if (!this.form.validate()) {
    return;
}

console.log(this.form.value);
```

You can find more examples [here](./packages/example/src/forms).
