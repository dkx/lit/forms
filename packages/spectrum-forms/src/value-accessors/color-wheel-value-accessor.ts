import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {ColorWheel} from '@spectrum-web-components/color-wheel';


export class ColorWheelValueAccessor implements FormControlValueAccessor<number>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	constructor(
		private readonly el: ColorWheel
	) {
		this.changeListener = () => {
			this.changed();
			this.touched();
		}

		this.el.addEventListener('change', this.changeListener);
	}

	public get valid(): boolean
	{
		return true;
	}

	public get value(): number|null
	{
		return this.el.value;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
	}

	public reportValidity(): boolean
	{
		return false;
	}

	public writeValue(value: number|null): void
	{
		if (value === null) {
			value = 0;
		}

		this.el.value = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return false;
	}
}

