import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {Checkbox} from '@spectrum-web-components/checkbox';


export class CheckboxValueAccessor implements FormControlValueAccessor<boolean>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	private _hasError: boolean|null = null;

	constructor(
		private readonly el: Checkbox,
	) {
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return true;
	}

	public get value(): boolean|null
	{
		return this.el.checked;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this._hasError = error !== null;
	}

	public reportValidity(): boolean
	{
		this.el.invalid = this._hasError;
		this._hasError = null;
		return true;
	}

	public writeValue(value: boolean|null): void
	{
		this.el.checked = value === true;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return !this.el.checked;
	}
}
