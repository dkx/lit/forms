import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {Slider} from '@spectrum-web-components/slider';


export class SliderValueAccessor implements FormControlValueAccessor<number>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly inputListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: Slider
	) {
		this.inputListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('input', this.inputListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		if (this.isEmpty()) {
			return true;
		}

		return !isNaN(this.el.value);
	}

	public get value(): number|null
	{
		return this.el.value;
	}

	public disconnect()
	{
		this.el.removeEventListener('input', this.inputListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
	}

	public reportValidity(): boolean
	{
		return false;
	}

	public writeValue(value: number|null): void
	{
		if (value === null) {
			value = 10;
		}

		this.el.value = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return false;
	}
}
