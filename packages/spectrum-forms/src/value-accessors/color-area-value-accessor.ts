import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {ColorArea} from '@spectrum-web-components/color-area';
import type {ColorValue} from '@spectrum-web-components/color-handle';


export class ColorAreaValueAccessor implements FormControlValueAccessor<ColorValue>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	constructor(
		private readonly el: ColorArea
	) {
		this.changeListener = () => {
			this.changed();
			this.touched();
		}

		this.el.addEventListener('change', this.changeListener);
	}

	public get valid(): boolean|null
	{
		return true;
	}

	public get value(): ColorValue
	{
		return this.el.value;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
	}

	public reportValidity(): boolean
	{
		return false;
	}

	public writeValue(value: ColorValue|null): void
	{
		if (value === null) {
			value = {
				h: 0,
				s: 100,
				l: 50,
			};
		}

		this.el.color = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return false;
	}
}

