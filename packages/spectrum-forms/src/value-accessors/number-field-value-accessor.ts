import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {NumberField} from '@spectrum-web-components/number-field';


export class NumberFieldValueAccessor implements FormControlValueAccessor<number>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly inputListener: () => void;

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	private _hasError: boolean|null = null;

	constructor(
		private readonly el: NumberField
	) {
		this.inputListener = () => this.changed();
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('input', this.inputListener);
		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		if (this.isEmpty()) {
			return true;
		}

		return !isNaN(this.el.value);
	}

	public get value(): number|null
	{
		const value = this.el.value;

		if (isNaN(value)) {
			return null;
		}

		return value;
	}

	public disconnect()
	{
		this.el.removeEventListener('input', this.inputListener);
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this._hasError = error !== null;
	}

	public reportValidity(): boolean
	{
		this.el.invalid = this._hasError;
		this._hasError = null;
		return true;
	}

	public writeValue(value: number|null): void
	{
		if (value === null) {
			this.el.valueAsString = '';
			return;
		}

		this.el.value = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return isNaN(this.el.value);
	}
}
