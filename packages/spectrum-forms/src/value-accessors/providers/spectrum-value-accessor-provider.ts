import type {FormControlValueAccessor, ValueAccessorProvider, ValueAccessorsRegistry} from '@dkx/lit-forms';
import type {Checkbox} from '@spectrum-web-components/checkbox';
import type {ColorArea} from '@spectrum-web-components/color-area';
import type {ColorSlider} from '@spectrum-web-components/color-slider';
import type {ColorWheel} from '@spectrum-web-components/color-wheel';
import type {NumberField} from '@spectrum-web-components/number-field';
import type {Picker} from '@spectrum-web-components/picker';
import type {RadioGroup} from '@spectrum-web-components/radio';
import type {Slider} from '@spectrum-web-components/slider';
import type {Switch} from '@spectrum-web-components/switch';
import type {Textfield} from '@spectrum-web-components/textfield';

import {CheckboxValueAccessor} from '../checkbox-value-accessor.js';
import {ColorAreaValueAccessor} from '../color-area-value-accessor.js';
import {ColorSliderValueAccessor} from '../color-slider-value-accessor.js';
import {ColorWheelValueAccessor} from '../color-wheel-value-accessor.js';
import {NumberFieldValueAccessor} from '../number-field-value-accessor.js';
import {PickerValueAccessor} from '../picker-value-accessor.js';
import {RadioGroupValueAccessor} from '../radio-group-value-accessor.js';
import {SliderValueAccessor} from '../slider-value-accessor.js';
import {SwitchValueAccessor} from '../switch-value-accessor.js';
import {TextfieldValueAccessor} from '../textfield-value-accessor.js';


export class SpectrumValueAccessorProvider implements ValueAccessorProvider
{
	public create(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null
	{
		if (isCheckbox(el)) {
			return new CheckboxValueAccessor(el);
		}

		if (isColorArea(el)) {
			return new ColorAreaValueAccessor(el);
		}

		if (isColorSlider(el)) {
			return new ColorSliderValueAccessor(el);
		}

		if (isColorWheel(el)) {
			return new ColorWheelValueAccessor(el);
		}

		if (isNumberField(el)) {
			return new NumberFieldValueAccessor(el);
		}

		if (isPicker(el)) {
			return new PickerValueAccessor(el);
		}

		if (isRadioGroup(el)) {
			return new RadioGroupValueAccessor(el);
		}

		if (isSlider(el)) {
			return new SliderValueAccessor(el);
		}

		if (isSwitch(el)) {
			return new SwitchValueAccessor(el);
		}

		if (isTextfield(el)) {
			return new TextfieldValueAccessor(el);
		}

        return null;
	}
}

function isCheckbox(el: Element): el is Checkbox
{
	return el.tagName === 'SP-CHECKBOX';
}

function isColorArea(el: Element): el is ColorArea
{
	return el.tagName === 'SP-COLOR-AREA';
}

function isColorSlider(el: Element): el is ColorSlider
{
	return el.tagName === 'SP-COLOR-SLIDER';
}

function isColorWheel(el: Element): el is ColorWheel
{
	return el.tagName === 'SP-COLOR-WHEEL';
}

function isNumberField(el: Element): el is NumberField
{
	return el.tagName === 'SP-NUMBER-FIELD';
}

function isPicker(el: Element): el is Picker
{
	return el.tagName === 'SP-PICKER';
}

function isRadioGroup(el: Element): el is RadioGroup
{
	return el.tagName === 'SP-RADIO-GROUP';
}

function isSlider(el: Element): el is Slider
{
	return el.tagName === 'SP-SLIDER';
}

function isSwitch(el: Element): el is Switch
{
	return el.tagName === 'SP-SWITCH';
}

function isTextfield(el: Element): el is Textfield
{
	return el.tagName === 'SP-TEXTFIELD';
}
