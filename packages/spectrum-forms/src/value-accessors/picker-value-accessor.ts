import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';
import type {Picker} from '@spectrum-web-components/picker';


export class PickerValueAccessor implements FormControlValueAccessor<string>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	private _hasError: boolean|null = null;

	constructor(
		private readonly el: Picker,
	) {
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean|null
	{
		return true;
	}

	public get value(): string
	{
		if (this.el.value === '') {
			for (let item of this.el.menuItems) {
				if (item.value === '') {
					return '';
				}
			}

			return null;
		}

		return this.el.value;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this._hasError = error !== null;
	}

	public reportValidity(): boolean
	{
		this.el.invalid = this._hasError;
		this._hasError = null;
		return true;
	}

	public writeValue(value: string|null): void
	{
		if (value === null) {
			value = '';
		}

		this.el.value = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return this.el.value.length === 0;
	}
}
