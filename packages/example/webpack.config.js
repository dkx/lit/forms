const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackTagsPlugin = require('html-webpack-tags-plugin');
const path = require('path');


module.exports = {
	mode: 'development',
	entry: {
		app: path.resolve(__dirname, 'src', 'main.ts'),
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].js',
		publicPath: '/',
		clean: true,
	},
	devServer: {
		contentBase: path.resolve(__dirname, 'dist'),
		port: 50000,
		open: true,
	},
	devtool: 'eval-source-map',
	resolve: {
		extensions: ['.js', '.ts'],
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				loader: 'ts-loader',
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: '@dkx/lit-forms',
		}),
		new HtmlWebpackTagsPlugin({
			links: [
				{
					path: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500',
					publicPath: false,
				},
				{
					path: 'https://fonts.googleapis.com/css?family=Material+Icons&display=block',
					publicPath: false,
				},
			],
		}),
	],
};
