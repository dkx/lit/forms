import {html} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {required, ValueAccessorProvider} from '@dkx/lit-forms';
import {MaterialValueAccessorProvider} from '@dkx/lit-forms-material';

import {BaseForm} from './base-form';

import '@material/mwc-button';
import '@material/mwc-checkbox';
import '@material/mwc-formfield';
import '@material/mwc-textfield';
import '@material/mwc-textarea';
import '@material/mwc-slider';
import '@material/mwc-switch';
import '@material/mwc-select';
import '@material/mwc-radio';
import '@material/mwc-fab';
import '@material/mwc-circular-progress';
import '@material/mwc-list/mwc-check-list-item.js';
import '@material/mwc-list/mwc-list.js';


@customElement('lf-material-form')
export class MaterialForm extends BaseForm
{
	constructor()
	{
		super();
	}

	protected createValueAccessorProvider(): ValueAccessorProvider
	{
		return new MaterialValueAccessorProvider();
	}

	protected createForms(): void
	{
		this.createForm2('checkbox');
		this.createForm2('checkboxRequired', required('Check me'));
		this.createForm2('listMultiple');
		this.createForm2('listMultipleRequired', required('Select something'));
		this.createForm2('radio');
		this.createForm2('radioRequired', required('Select option'));
		this.createForm2('select');
		this.createForm2('selectRequired', required('Select option'));
		this.createForm2('slider');
		this.createForm2('switch');
		this.createForm2('switchRequired', required('Check me'));
		this.createForm2('textarea');
		this.createForm2('textareaRequired', required('Type something'));
		this.createForm2('textfield');
		this.createForm2('textfieldRequired', required('Type something'));
		this.createForm2('textfieldNumber');
	}

	protected render(): unknown
	{
		if (this.redrawingForms) {
			return html`
				<mwc-circular-progress indeterminate></mwc-circular-progress>
			`;
		}

		return html`
			<sp-theme>
				<div class="form">
					${this.forms.checkbox.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Checkbox">
							<mwc-checkbox ${form.attach('value')}></mwc-checkbox>
						</mwc-formfield>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.checkboxRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Checkbox">
							<mwc-checkbox ${form.attach('value')} required></mwc-checkbox>
						</mwc-formfield>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.listMultiple.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							List / multiple
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-list ${form.attach('value')} multi>
							<mwc-check-list-item value="option-1">Option 1</mwc-check-list-item>
							<mwc-check-list-item value="option-2">Option 2</mwc-check-list-item>
							<mwc-check-list-item value="option-3">Option 3</mwc-check-list-item>
						</mwc-list>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.listMultipleRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							List / multiple required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-list ${form.attach('value')} multi>
							<mwc-check-list-item value="option-1">Option 1</mwc-check-list-item>
							<mwc-check-list-item value="option-2">Option 2</mwc-check-list-item>
							<mwc-check-list-item value="option-3">Option 3</mwc-check-list-item>
						</mwc-list>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radio.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Option 1">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-1"></mwc-radio>
						</mwc-formfield>
						<mwc-formfield label="Option 2">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-2"></mwc-radio>
						</mwc-formfield>
						<mwc-formfield label="Option 3">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-3"></mwc-radio>
						</mwc-formfield>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radioRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Option 1">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-1"></mwc-radio>
						</mwc-formfield>
						<mwc-formfield label="Option 2">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-2"></mwc-radio>
						</mwc-formfield>
						<mwc-formfield label="Option 3">
							<mwc-radio ${form.attach('value')} name="material-radio" value="option-3"></mwc-radio>
						</mwc-formfield>
						<br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.select.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-select ${form.attach('value')} label="Select">
							<mwc-list-item value="option-1">Option 1</mwc-list-item>
							<mwc-list-item value="option-2">Option 2</mwc-list-item>
							<mwc-list-item value="option-3">Option 3</mwc-list-item>
						</mwc-select>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.selectRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-select ${form.attach('value')} label="Select">
							<mwc-list-item value="option-1">Option 1</mwc-list-item>
							<mwc-list-item value="option-2">Option 2</mwc-list-item>
							<mwc-list-item value="option-3">Option 3</mwc-list-item>
						</mwc-select>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.slider.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Slider
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Slider">
							<mwc-slider ${form.attach('value')} min="0" max="100" step="1"></mwc-slider>
						</mwc-formfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.switch.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Switch
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Switch">
							<mwc-switch ${form.attach('value')}></mwc-switch>
						</mwc-formfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.switchRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Switch / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-formfield label="Switch">
							<mwc-switch ${form.attach('value')}></mwc-switch>
						</mwc-formfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textarea.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textarea
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-textarea ${form.attach('value')} label="Textarea"></mwc-textarea>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textareaRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textarea / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-textarea ${form.attach('value')} label="Textarea" required></mwc-textarea>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<div class="form">
					${this.forms.textfield.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textfield
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-textfield ${form.attach('value')} type="text"></mwc-textfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textfieldRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textfield / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-textfield ${form.attach('value')} type="text" required></mwc-textfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textfieldNumber.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textfield / number
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<mwc-textfield ${form.attach('value')} type="number"></mwc-textfield>
						<br><br>
						<mwc-button raised label="Submit" @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}"></mwc-button>
					`)}
				</div>
			</sp-theme>
		`;
	}
}
