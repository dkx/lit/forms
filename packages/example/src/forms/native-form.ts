import {html} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {DefaultValueAccessorProvider, required, ValueAccessorProvider} from '@dkx/lit-forms';

import {BaseForm} from './base-form';


@customElement('lf-native-form')
export class NativeForm extends BaseForm
{
	constructor()
	{
		super();
	}

	protected createValueAccessorProvider(): ValueAccessorProvider
	{
		return new DefaultValueAccessorProvider();
	}

	protected createForms(): void
	{
		this.createForm2('checkbox');
		this.createForm2('checkboxRequired', required('Check this checkbox'));
		this.createForm2('numeric');
		this.createForm2('numericRequired', required('This is mandatory'));
		this.createForm2('range');
		this.createForm2('text');
		this.createForm2('radio');
		this.createForm2('radioRequired', required('Select an option'));
		this.createForm2('select');
		this.createForm2('selectEmptyDefault');
		this.createForm2('selectRequired', required('Select something'));
		this.createForm2('selectMultiple');
		this.createForm2('selectMultipleRequired', required('Select at least one option'));
		this.createForm2('textarea');
		this.createForm2('textareaRequired', required('Type something'));
	}

	protected render(): unknown
	{
		if (this.redrawingForms) {
			return html`
				<sp-progress-circle indeterminate size="small"></sp-progress-circle>
			`;
		}

		return html`
			<sp-theme>
				<div class="form">
					${this.forms.checkbox.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<label>
							<input ${form.attach('value')} type="checkbox"> Checkbox
						</label>
						<br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.checkboxRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<label>
							<input ${form.attach('value')} type="checkbox" required> Checkbox
						</label>
						<br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.numeric.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Numeric input
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<input ${form.attach('value')} type="number">
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.numericRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Numeric input / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<input ${form.attach('value')} type="number" required>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.range.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Range
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<input ${form.attach('value')} type="range" min="0" max="100">
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.text.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Input
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<input ${form.attach('value')} type="text">
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radio.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-1"> Option 1
						</label>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-2"> Option 2
						</label>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-3"> Option 3
						</label>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radioRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-1" required> Option 1
						</label>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-2" required> Option 2
						</label>
						<label>
							<input ${form.attach('value')} type="radio" name="native-radio" value="option-3" required> Option 3
						</label>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.select.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<select ${form.attach('value')}>
							<option value="option-1">Option 1</option>
							<option value="option-2">Option 2</option>
							<option value="option-3">Option 3</option>
						</select>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.selectEmptyDefault.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select / empty default
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<select ${form.attach('value')}>
							<option value="">Choose...</option>
							<option value="option-1">Option 1</option>
							<option value="option-2">Option 2</option>
							<option value="option-3">Option 3</option>
						</select>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.selectRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<select ${form.attach('value')} required>
							<option value="option-1">Option 1</option>
							<option value="option-2">Option 2</option>
							<option value="option-3">Option 3</option>
						</select>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.selectMultiple.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select / multiple
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<select ${form.attach('value')} multiple>
							<option value="option-1">Option 1</option>
							<option value="option-2">Option 2</option>
							<option value="option-3">Option 3</option>
						</select>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.selectMultipleRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Select / multiple required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<select ${form.attach('value')} multiple required>
							<option value="option-1">Option 1</option>
							<option value="option-2">Option 2</option>
							<option value="option-3">Option 3</option>
						</select>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textarea.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textarea
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<textarea ${form.attach('value')}></textarea>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textareaRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textarea / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<textarea ${form.attach('value')} required></textarea>
						<br><br>
						<button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</button>
					`)}
				</div>
			</sp-theme>
		`;
	}
}
