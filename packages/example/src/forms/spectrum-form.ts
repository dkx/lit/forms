import {html, PropertyValues} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {required, ValueAccessorProvider} from '@dkx/lit-forms';
import {SpectrumValueAccessorProvider} from '@dkx/lit-forms-spectrum';

import {BaseForm} from './base-form';

import '@spectrum-web-components/bundle/elements.js';


@customElement('lf-spectrum-form')
export class SpectrumForm extends BaseForm
{
	constructor()
	{
		super();
	}

	protected createValueAccessorProvider(): ValueAccessorProvider
	{
		return new SpectrumValueAccessorProvider();
	}

	protected createForms(): void
	{
		this.createForm2('checkbox');
		this.createForm2('checkboxRequired', required('Check this checkbox'));
		this.createForm2('colorArea');
		this.createForm2('colorSlider');
		this.createForm2('colorWheel');
		this.createForm2('number');
		this.createForm2('picker');
		this.createForm2('pickerRequired', required('Select item'));
		this.createForm2('radio');
		this.createForm2('radioRequired', required('Select item'));
		this.createForm2('slider');
		this.createForm2('switch');
		this.createForm2('textfield');
		this.createForm2('textfieldRequired', required('This field is required'));
	}

	protected render(): unknown
	{
		if (this.redrawingForms) {
			return html`
				<sp-progress-circle indeterminate size="small"></sp-progress-circle>
			`;
		}

		return html`
			<sp-theme>
				<div class="form">
					${this.forms.checkbox.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-checkbox ${form.attach('value')}>Checkbox</sp-checkbox><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.checkboxRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Checkbox / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-checkbox ${form.attach('value')}>Checkbox</sp-checkbox><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.colorArea.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Color area
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-color-area ${form.attach('value')}></sp-color-area><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.colorSlider.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Color slider
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-color-slider ${form.attach('value')}></sp-color-slider><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.colorWheel.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Color wheel
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-color-wheel ${form.attach('value')}></sp-color-wheel><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.number.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Number Field
							<overlay-trigger placement="top" offset="5">
								<sp-status-light slot="trigger" size="m" variant="negative">broken</sp-status-light>
								<sp-popover slot="click-content" direction="bottom" tip>
									<sp-dialog size="small">
										<h2 slot="heading">Bugs</h2>
										<ul>
											<li><sp-link href="https://github.com/adobe/spectrum-web-components/issues/1509">https://github.com/adobe/spectrum-web-components/issues/1509</sp-link></li>
										</ul>
									</sp-dialog>
								</sp-popover>
							</overlay-trigger>
						</h2>
						<sp-number-field ${form.attach('value')} style="width: 150px"></sp-number-field><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.picker.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Picker
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-picker ${form.attach('value')} size="m" label="Choose...">
							<sp-menu-item value="item-1">Item 1</sp-menu-item>
							<sp-menu-item value="item-2">Item 2</sp-menu-item>
							<sp-menu-item value="item-3">Item 3</sp-menu-item>
							<sp-menu-item value="item-4">Item 4</sp-menu-item>
						</sp-picker>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.pickerRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Picker / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-picker ${form.attach('value')} size="m" label="Choose...">
							<sp-menu-item value="item-1">Item 1</sp-menu-item>
							<sp-menu-item value="item-2">Item 2</sp-menu-item>
							<sp-menu-item value="item-3">Item 3</sp-menu-item>
							<sp-menu-item value="item-4">Item 4</sp-menu-item>
						</sp-picker>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radio.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-radio-group ${form.attach('value')}>
							<sp-radio value="first">Option 1</sp-radio>
							<sp-radio value="second">Option 2</sp-radio>
							<sp-radio value="third">Option 3</sp-radio>
							<sp-radio value="fourth">Option 4</sp-radio>
						</sp-radio-group>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.radioRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Radio / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-radio-group ${form.attach('value')}>
							<sp-radio value="first">Option 1</sp-radio>
							<sp-radio value="second">Option 2</sp-radio>
							<sp-radio value="third">Option 3</sp-radio>
							<sp-radio value="fourth">Option 4</sp-radio>
						</sp-radio-group>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.slider.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Slider
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-slider ${form.attach('value')} label="Slider"></sp-slider>
						<br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.switch.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Switch
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-switch ${form.attach('value')} label="Switch">Switch</sp-switch>
						<br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textfield.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textfield
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-textfield ${form.attach('value')}></sp-textfield>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>

				<sp-divider size="l"></sp-divider>

				<div class="form">
					${this.forms.textfieldRequired.render(form => html`
						<h2 class="spectrum-Heading spectrum-Heading--sizeXS">
							Textfield / required
							<sp-status-light size="m" variant="positive">works</sp-status-light>
						</h2>
						<sp-textfield ${form.attach('value')}></sp-textfield>
						<br><br>
						<sp-button @click="${e => this.submitForm(e.target, form)}" ?disabled="${this.shouldDisableSubmit(form)}">Submit</sp-button>
					`)}
				</div>
			</sp-theme>
		`;
	}

	protected updated(_changedProperties: PropertyValues)
	{
		super.updated(_changedProperties);
	}
}
