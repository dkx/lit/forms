import {css, LitElement, PropertyValues} from 'lit';
import {state} from 'lit/decorators/state.js';
import {property} from 'lit/decorators/property.js';
import {Button} from '@spectrum-web-components/button';
import {Overlay} from '@spectrum-web-components/overlay';
import {Tray} from '@spectrum-web-components/tray';
import {Form, FormBuilder, FormControlValidator, FormGroupBuilderOptions, ValueAccessorProvider} from '@dkx/lit-forms';


export abstract class BaseForm extends LitElement
{
	public static styles = css`
		sp-status-light {
			width: fit-content;
		}
		.form {
			padding: 25px;
		}
	`;

	@state()
	protected expandedTitles: boolean = false;

	@property({attribute: false})
	public disableWhenInvalid: boolean = false;

	@property({attribute: false})
	public reportErrorsOnTouched: boolean = true;

	@property({attribute: false})
	public reportErrorsOnChanged: boolean = true;

	private _initialized: boolean = false;

	@state()
	protected redrawingForms: boolean = false;

	@state()
	protected forms: Record<string, Form<any>> = {};

	private readonly formBuilder: FormBuilder;

	protected constructor()
	{
		super();

		this.formBuilder = new FormBuilder(this.createValueAccessorProvider());
		this.createForms();
	}

	protected abstract createValueAccessorProvider(): ValueAccessorProvider;

	protected abstract createForms(): void;

	public willUpdate(changes: PropertyValues<BaseForm>): void
	{
		if (!this._initialized) {
			return;
		}

		if (changes.has('reportErrorsOnTouched') || changes.has('reportErrorsOnChanged')) {
			this.redrawingForms = true;

			for (let name in this.forms) {
				if (this.forms.hasOwnProperty(name)) {
					this.forms[name].destroy();
				}
			}

			this.forms = {};

			this.updateComplete.then(() => {
				this.redrawingForms = false;

				if (Object.keys(this.forms).length === 0) {
					this.createForms();
				}
			});
		}
	}

	protected firstUpdated(changes: PropertyValues<BaseForm>)
	{
		super.firstUpdated(changes);
		this._initialized = true;
	}

	public resetForm(): void
	{
		for (let key in this.forms) {
			if (this.forms.hasOwnProperty(key)) {
				this.forms[key].reset();
			}
		}
	}

	protected shouldDisableSubmit(form: Form<any>): boolean
	{
		return this.disableWhenInvalid && !form.valid
	}

	private createFormOptions(): FormGroupBuilderOptions
	{
		return {
			reportErrorsOnTouched: this.reportErrorsOnTouched,
			reportErrorsOnChanged: this.reportErrorsOnChanged,
		};
	}

	protected createForm2(name: string, ...controlValidators: ReadonlyArray<FormControlValidator<any>>): void
	{
		this.forms[name] = this.formBuilder.create(this, root => [
			root.control('value', null, {
				validators: controlValidators,
			}),
		], this.createFormOptions());
	}

	protected submitForm(trigger: Button, form: Form<any>): void
	{
		if (!form.validate()) {
			return;
		}

		const wrapper = document.createElement('div');
		wrapper.innerHTML = `
			<sp-tray>
				<sp-dialog size="small" dismissable>
					<h2 slot="heading">Value</h2>
					<pre><code>${JSON.stringify(form.value, null, 2)}</code></pre>
				</sp-dialog>
			</sp-tray>
		`.trim();

		Overlay.open(trigger, 'modal', wrapper.firstChild as Tray, {
			placement: 'none',
		});
	}
}
