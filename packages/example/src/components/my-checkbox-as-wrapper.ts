import {html, LitElement,} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {query} from 'lit/decorators/query.js';
import {property} from 'lit/decorators/property.js';
import {
	ComponentValueAccessorFactory,
	FormControlValueAccessor, ValueAccessorsRegistry,
} from '@dkx/lit-forms';
import {CheckboxValueAccessor} from '@dkx/lit-forms/lib/value-accessors/defaults/checkbox-value-accessor';


@customElement('my-checkbox-as-wrapper')
export class MyCheckboxAsWrapper extends LitElement implements ComponentValueAccessorFactory<boolean>
{
	@property({type: String})
	public label: string;

	@property({type: Boolean})
	public required: boolean = false;

	@query('input', true)
	protected readonly el: HTMLInputElement;

	public createValueAccessor(registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<boolean>
	{
		if (typeof this.el === 'undefined') {
			throw new Error('Component is not ready yet');
		}

		return new CheckboxValueAccessor(this.el);
	}

	protected render(): unknown
	{
		return html`
			<label>
				<input type="checkbox" required ?required="${this.required}">
				${this.label}
			</label>
		`;
	}
}
