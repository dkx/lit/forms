import {html, LitElement} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {query} from 'lit/decorators/query.js';
import {property} from 'lit/decorators/property';
import {
	ComponentValueAccessorFactory,
	FormControlChanged,
	FormControlError,
	FormControlTouched,
	FormControlValueAccessor, ValueAccessorsRegistry,
} from '@dkx/lit-forms';


@customElement('my-checkbox')
export class MyCheckbox extends LitElement implements ComponentValueAccessorFactory<boolean>, FormControlValueAccessor<boolean>
{
	@property({type: String})
	public label: string;

	@property({type: Boolean})
	public required: boolean = false;

	@query('input', true)
	protected readonly el: HTMLInputElement;

	public readonly supportsMultipleElements: boolean = false;

	private changeListener: () => void;

	private blurListener: () => void;

	public get valid(): boolean
	{
		return this.el?.validity.valid ?? false;
	}

	public get value(): boolean
	{
		return this.el?.checked ?? false;
	}

	public createValueAccessor(registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<boolean>
	{
		return this;
	}

	public disconnect(): void
	{
		this.el?.removeEventListener('change', this.changeListener);
		this.el?.removeEventListener('blur', this.blurListener);
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		if (typeof this.el === 'undefined') {
			console.warn('Component is not ready yet');
			return;
		}

		this.changeListener = () => fn();

		this.el.addEventListener('change', this.changeListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		if (typeof this.el === 'undefined') {
			console.warn('Component is not ready yet');
			return;
		}

		this.blurListener = fn;
		this.el.addEventListener('blur', this.blurListener);
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el?.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		this.el?.reportValidity();
		return true;
	}

	public writeValue(value: boolean|null): void
	{
		if (typeof this.el !== 'undefined') {
			this.el.checked = value === true;
		}
	}

	public setDisabledState(state: boolean): void
	{
		if (typeof this.el !== 'undefined') {
			this.el.disabled = state;
		}
	}

	public isEmpty(): boolean
	{
		return (!this.el?.checked) ?? false;
	}

	protected render(): unknown
	{
		return html`
			<label>
				<input type="checkbox" ?required="${this.required}">
				${this.label}
			</label>
		`;
	}
}
