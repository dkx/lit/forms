import {LitElement, html, css} from 'lit';
import {customElement} from 'lit/decorators/custom-element.js';
import {state} from 'lit/decorators/state.js';
import {BaseForm} from './forms/base-form';

import '@material/mwc-tab';
import '@material/mwc-tab-bar';
import '@material/mwc-drawer';
import '@spectrum-web-components/bundle/elements.js';

import './forms/native-form';
import './forms/material-form';
import './forms/spectrum-form';


@customElement('lf-app')
export class App extends LitElement
{
	public static styles = css`
		:host {
			display: block;
			width: 100%;
		}
		.content {
			padding: 25px;
		}
	`;

	@state()
	private active: number = 0;

	constructor()
	{
		super();

		const hash = /^#(\d)$/.exec(window.location.hash);
		if (hash !== null) {
			this.active = parseInt(hash[1]);
		}
	}

	private get tabs(): ReadonlyArray<{title: string, content: unknown}>
	{
		return [
			{
				title: 'Native',
				content: html`<lf-native-form class="lf-form"></lf-native-form>`,
			},
			{
				title: 'Material',
				content: html`<lf-material-form class="lf-form"></lf-material-form>`,
			},
			{
				title: 'Spectrum',
				content: html`<lf-spectrum-form class="lf-form"></lf-spectrum-form>`,
			},
		];
	}

	private get form(): BaseForm
	{
		return this.renderRoot.querySelector('.lf-form') as BaseForm;
	}

	protected render(): unknown
	{
		return html`
			<mwc-drawer>
				<div>
					<mwc-button raised label="Reset" @click="${() => this.form.resetForm()}"></mwc-button><br><br>
					<mwc-formfield label="Disable submit button when invalid">
						<mwc-checkbox @change="${e => this.form.disableWhenInvalid = e.target.checked}"></mwc-checkbox>
					</mwc-formfield><br><br>
					<mwc-formfield label="Report errors on touched">
						<mwc-checkbox checked @change="${e => this.form.reportErrorsOnTouched = e.target.checked}"></mwc-checkbox>
					</mwc-formfield>
					<mwc-formfield label="Report errors on changed">
						<mwc-checkbox checked @change="${e => this.form.reportErrorsOnChanged = e.target.checked}"></mwc-checkbox>
					</mwc-formfield>
				</div>
				<div slot="appContent">
					<mwc-tab-bar @MDCTabBar:activated="${this.handleTabActivated}" activeIndex="${this.active}">
						${this.tabs.map(tab => html`
							<mwc-tab label="${tab.title}"></mwc-tab>
						`)}
					</mwc-tab-bar>
					<div class="content">
						${this.tabs[this.active].content}
					</div>
				</div>
			</mwc-drawer>
		`;
	}

	private handleTabActivated(e: CustomEvent<{index: number}>): void
	{
		this.active = e.detail.index;
		window.location.href = '#' + this.active;
	}
}

declare global
{
	interface HTMLElementTagNameMap
	{
		"lf-app": App,
	}
}
