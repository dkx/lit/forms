
// https://github.com/material-components/material-components-web-components/issues/971
export function tryInjectNativeValidationMessage(el: any): void
{
	el.validationMessage = (el as { formElement: HTMLInputElement }).formElement.validationMessage;
}
