import type {TextField} from '@material/mwc-textfield';
import type {
	FormControlChanged,
	FormControlError,
	FormControlTouched,
	FormControlValueAccessor
} from '@dkx/lit-forms';

import {tryInjectNativeValidationMessage} from '../utils.js';


export class TextfieldNumericValueAccessor implements FormControlValueAccessor<number>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly inputListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: TextField,
	) {
		this.inputListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('input', this.inputListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return this.el.validity.valid;
	}

	public get value(): number|null
	{
		return parseFloat(this.el.value);
	}

	public disconnect()
	{
		this.el.removeEventListener('input', this.inputListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		tryInjectNativeValidationMessage(this.el);
		this.el.reportValidity();
		return true;
	}

	public writeValue(value: number|null): void
	{
		if (value === null) {
			this.el.value = '';
			return;
		}

		let val: string = '';

		if (typeof value === 'string') {
			val = value;
		} else {
			val = value.toString(10);
		}

		this.el.value = val;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return this.el.value.length === 0;
	}
}
