import type {Select} from '@material/mwc-select';
import type {
	FormControlChanged,
	FormControlError,
	FormControlTouched,
	FormControlValueAccessor
} from '@dkx/lit-forms';

import {tryInjectNativeValidationMessage} from '../utils.js';


export class SelectValueAccessor implements FormControlValueAccessor<string>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: Select,
	) {
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return this.el.validity.valid;
	}

	public get value(): string|null
	{
		return this.el.value;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		tryInjectNativeValidationMessage(this.el);
		this.el.reportValidity();
		return true;
	}

	public writeValue(value: string|null): void
	{
		if (value === null) {
			value = '';
		}

		this.el.value = value;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return this.el.value.length === 0;
	}
}
