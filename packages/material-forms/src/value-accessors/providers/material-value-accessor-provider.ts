import type {FormControlValueAccessor, ValueAccessorProvider, ValueAccessorsRegistry} from '@dkx/lit-forms';
import type {TextField} from '@material/mwc-textfield';
import type {Checkbox} from '@material/mwc-checkbox';
import type {Radio} from '@material/mwc-radio';
import type {Select} from '@material/mwc-select';
import type {List} from '@material/mwc-list';
import type {TextArea} from '@material/mwc-textarea';
import type {Slider} from '@material/mwc-slider';
import type {Switch} from '@material/mwc-switch';

import {TextfieldValueAccessor} from '../textfield-value-accessor.js';
import {CheckboxValueAccessor} from '../checkbox-value-accessor.js';
import {RadioValueAccessor} from '../radio-value-accessor.js';
import {SelectValueAccessor} from '../select-value-accessor.js';
import {ListMultipleValueAccessor} from '../list-multiple-value-accessor.js';
import {TextareaValueAccessor} from '../textarea-value-accessor.js';
import {SliderValueAccessor} from '../slider-value-accessor.js';
import {TextfieldNumericValueAccessor} from '../textfield-numeric-value-accessor.js';
import {SwitchValueAccessor} from '../switch-value-accessor.js';


export class MaterialValueAccessorProvider implements ValueAccessorProvider
{
	public create<TValue>(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null
	{
		if (isTextField(el)) {
			if (el.type === 'number') {
				return new TextfieldNumericValueAccessor(el);
			}

			return new TextfieldValueAccessor(el);
		}

		if (isCheckbox(el)) {
			return new CheckboxValueAccessor(el);
		}

		if (isRadio(el)) {
			return new RadioValueAccessor(el, registry);
		}

		if (isSelect(el)) {
			return new SelectValueAccessor(el);
		}

		if (isList(el) && el.multi) {
			return new ListMultipleValueAccessor(el);
		}

		if (isTextArea(el)) {
			return new TextareaValueAccessor(el);
		}

		if (isSlider(el)) {
			return new SliderValueAccessor(el);
		}

		if (isSwitch(el)) {
			return new SwitchValueAccessor(el);
		}

		return null;
	}
}

function isTextField(el: Element): el is TextField
{
	return el.tagName === 'MWC-TEXTFIELD';
}

function isCheckbox(el: Element): el is Checkbox
{
	return el.tagName === 'MWC-CHECKBOX';
}

function isRadio(el: Element): el is Radio
{
	return el.tagName === 'MWC-RADIO';
}

function isSelect(el: Element): el is Select
{
	return el.tagName === 'MWC-SELECT';
}

function isList(el: Element): el is List
{
	return el.tagName === 'MWC-LIST';
}

function isTextArea(el: Element): el is TextArea
{
	return el.tagName === 'MWC-TEXTAREA';
}

function isSlider(el: Element): el is Slider
{
	return el.tagName === 'MWC-SLIDER';
}

function isSwitch(el: Element): el is Switch
{
	return el.tagName === 'MWC-SWITCH';
}
