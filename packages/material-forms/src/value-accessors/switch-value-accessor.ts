import type {FormControlError, FormControlValueAccessor, FormControlChanged, FormControlTouched} from '@dkx/lit-forms';
import type {Switch} from '@material/mwc-switch';


export class SwitchValueAccessor implements FormControlValueAccessor<boolean>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: Switch,
	) {
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return true;
	}

	public get value(): boolean|null
	{
		return this.el.checked;
	}

	public disconnect(): void
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
	}

	public reportValidity(): boolean
	{
		return false;
	}

	public writeValue(value: boolean|null): void
	{
		this.el.checked = value === true;
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return !this.el.checked;
	}
}
