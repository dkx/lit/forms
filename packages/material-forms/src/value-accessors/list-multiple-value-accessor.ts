import type {List} from '@material/mwc-list';
import type {ListItemBase} from '@material/mwc-list/mwc-list-item-base';
import type {MultiSelectedEvent} from '@material/mwc-list/mwc-list-foundation';
import type {FormControlChanged, FormControlError, FormControlTouched, FormControlValueAccessor} from '@dkx/lit-forms';


export class ListMultipleValueAccessor implements FormControlValueAccessor<ReadonlyArray<string>>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly selectedListener: (e: MultiSelectedEvent) => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: List,
	) {
		this.selectedListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('selected', this.selectedListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return true;
	}

	public get value(): ReadonlyArray<string>|null
	{
		const values: Array<string> = [];
		for (let index of (this.el.index as Set<number>)) {
			values.push(this.el.items[index].value);
		}

		return values;
	}

	public disconnect()
	{
		this.el.removeEventListener('selected', this.selectedListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
	}

	public reportValidity(): boolean
	{
		return false;
	}

	public writeValue(value: ReadonlyArray<string>|null): void
	{
		if (value === null) {
			for (let item of this.el.items) {
				item.selected = false;
			}

			return;
		}

		for (let item of this.el.items) {
			item.selected = value.indexOf(item.value) >= 0;
		}
	}

	public setDisabledState(state: boolean): void
	{
		for (let item of this.el.items) {
			item.disabled = state;
		}
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return (this.el.selected as Array<ListItemBase>).length === 0;
	}
}
