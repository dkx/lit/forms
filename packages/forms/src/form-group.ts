import type {FormMember} from './form-member.js';
import type {FormBuilderMembersFactory} from './builders/form-builder.js';
import type {FormParent} from './form-parent.js';
import type {FormGroupBuilderFactory} from './builders/form-group-builder-factory.js';
import {FormBaseGroup} from './form-base-group.js';


export class FormGroup<TItems> extends FormBaseGroup<TItems> implements FormMember<TItems>
{
	constructor(
		name: string,
		parent: FormParent,
		groupBuilderFactory: FormGroupBuilderFactory,
		factory: FormBuilderMembersFactory,
	)
	{
		super(name, parent, groupBuilderFactory, factory);
	}

	/** @internal */
	public destroy(): void
	{
		super.destroy();

		for (let key in this.controls) {
			if (this.controls.hasOwnProperty(key)) {
				this.controls[key].destroy();
			}
		}
	}

	public render(content: (group: FormGroup<TItems>) => unknown): unknown
	{
		return content(this);
	}
}
