import type {ReactiveController, ReactiveControllerHost} from 'lit';
import type {ItemTemplate} from 'lit-html/directives/repeat.js';
import type {Observable, Subscription} from 'rxjs';
import {repeat} from 'lit/directives/repeat.js';
import {Subject} from 'rxjs';

import type {FormMember} from './form-member.js';
import type {FormGroup} from './form-group.js';
import type {FormParent} from './form-parent.js';
import type {FormControl} from './form-control.js';
import type {FormGroupBuilderFactory} from './builders/form-group-builder-factory.js';
import type {FormGroupBuilder} from './builders/form-group-builder.js';
import type {FormBuilderMembersFactory} from './builders/form-builder.js';


export class FormArray<TItems> implements FormParent, FormMember<ReadonlyArray<TItems>>, ReactiveController
{
	private readonly _valueChanges: Subject<ReadonlyArray<TItems>> = new Subject();

	private subscriptions: WeakMap<FormGroup<TItems>, Subscription> = new WeakMap();

	private _groups: Array<FormGroup<TItems>> = [];

	private _value: Array<TItems> = [];

	private _valid: boolean = true;

	private readonly groupBuilder: FormGroupBuilder;

	private currentId: number = 0;

	constructor(
		private readonly host: ReactiveControllerHost,
		public readonly name: string,
		private readonly parent: FormParent,
		groupBuilderFactory: FormGroupBuilderFactory,
	)
	{
		this.groupBuilder = groupBuilderFactory.create(this);
		this.host.addController(this);
	}

	public get value(): ReadonlyArray<TItems>|null
	{
		return this._value;
	}
	public set value(value: ReadonlyArray<TItems>|null)
	{
		if (value === null) {
			return;
		}

		if (value.length !== this._groups.length) {
			throw new Error(`Can not set value of FormArray "${this.name}" because it contains ${this._groups.length} groups, but ${value.length} were provided`);
		}

		for (let i = 0; i < value.length; i++) {
			this._value[i] = value[i];
			this._groups[i].patchValues(value[i]);
			this._valid = this.checkValidState();
		}

		this._valueChanges.next(this._value);
	}

	public get valueChanges(): Observable<ReadonlyArray<TItems>>
	{
		return this._valueChanges;
	}

	public get valid(): boolean
	{
		return this._valid;
	}

	public get groups(): ReadonlyArray<FormGroup<TItems>>
	{
		return this._groups;
	}

	public addGroup(factory: FormBuilderMembersFactory): FormGroup<TItems>
	{
		const group = this.groupBuilder.group<TItems>((this.currentId++).toString(10), factory);

		this._groups.push(group);
		this._value.push(group.value);
		this._valid = this.checkValidState();
		this._valueChanges.next(this._value);

		this.subscriptions.set(group, group.valueChanges.subscribe(value => {
			const pos = this._groups.indexOf(group);
			if (pos >= 0) {
				this._value[pos] = value;
				this._valid = this.checkValidState();
				this._valueChanges.next(this._value);
			}
		}));

		return group;
	}

	public removeGroup(group: FormGroup<TItems>): void
	{
		group.destroy();

		const pos = this._groups.indexOf(group);
		if (pos >= 0) {
			this._groups.splice(pos, 1);
			this._value.splice(pos, 1);
			this._valid = this.checkValidState();
			this._valueChanges.next(this._value);
		}

		if (this.subscriptions.has(group)) {
			this.subscriptions.get(group).unsubscribe();
			this.subscriptions.delete(group);
		}
	}

	public render(content: ItemTemplate<FormGroup<TItems>>): unknown
	{
		return repeat(this._groups, g => g.name, content);
	}

	/** @internal */
	public destroy(): void
	{
		this._groups.forEach(group => {
			if (this.subscriptions.has(group)) {
				this.subscriptions.get(group).unsubscribe();
				this.subscriptions.delete(group);
			}

			group.destroy();
		});

		this._valueChanges.complete();
	}

	/** @internal */
	public getControls(): ReadonlyArray<FormControl<any>>
	{
		return this._groups.reduce((data, current) => [...data, ...current.getControls()], []);
	}

	/** @internal */
	public hostUpdated(): void
	{
	}

	private checkValidState(): boolean
	{
		for (let group of this._groups) {
			if (!group.valid) {
				return false;
			}
		}

		return true;
	}
}
