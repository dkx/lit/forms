import type {FormControl} from '../form-control.js';
import type {FormControlError} from './form-control-error.js';


export type FormControlValidator<TValue> = (control: FormControl<TValue>) => FormControlError|null;
