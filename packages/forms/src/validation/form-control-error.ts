export class FormControlError
{
	constructor(
		public readonly message: string,
	) {}
}
