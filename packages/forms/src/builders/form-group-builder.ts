import type {ReactiveControllerHost} from 'lit';

import type {FormParent} from '../form-parent.js';
import type {FormControlOptions} from '../form-control.js';
import type {FormBuilderMembersFactory} from './form-builder.js';
import type {ValueAccessorProvider} from '../value-accessors/providers/value-accessor-provider.js';
import type {FormGroupBuilderFactory} from './form-group-builder-factory.js';
import {FormGroup} from '../form-group.js';
import {FormControl} from '../form-control.js';
import {FormArray} from '../form-array.js';


export declare interface FormGroupBuilderOptions
{
	reportErrorsOnTouched?: boolean,
	reportErrorsOnChanged?: boolean,
}

export class FormGroupBuilder
{
	private readonly _reportErrorsOnTouched: boolean;

	private readonly _reportErrorsOnChanged: boolean;

	constructor(
		private readonly host: ReactiveControllerHost,
		private readonly factory: FormGroupBuilderFactory,
		private readonly valueAccessorProvider: ValueAccessorProvider,
		private readonly parent: FormParent,
		options: FormGroupBuilderOptions,
	) {
		this._reportErrorsOnTouched = options.reportErrorsOnTouched ?? true;
		this._reportErrorsOnChanged = options.reportErrorsOnChanged ?? true;
	}

	public group<TItems>(name: string, factory: FormBuilderMembersFactory): FormGroup<TItems>
	{
		return new FormGroup<TItems>(name, this.parent, this.factory, factory);
	}

	public array<TItems>(name: string): FormArray<TItems>
	{
		return new FormArray<TItems>(this.host, name, this.parent, this.factory);
	}

	public control<TValue>(
		name: string,
		defaultValue: TValue|null,
		options: FormControlOptions<TValue> = {},
	)
	{
		if (typeof options.reportErrorsOnTouched === 'undefined') {
			options.reportErrorsOnTouched = this._reportErrorsOnTouched;
		}

		if (typeof options.reportErrorsOnChanged === 'undefined') {
			options.reportErrorsOnChanged = this._reportErrorsOnChanged;
		}

		return new FormControl<TValue>(this.host, this.valueAccessorProvider, this.parent, name, defaultValue, options);
	}
}
