import type {ReactiveControllerHost} from 'lit';

import type {ValueAccessorProvider} from '../value-accessors/providers/value-accessor-provider.js';
import type {FormGroupBuilder, FormGroupBuilderOptions} from './form-group-builder.js';
import type {FormMember} from '../form-member.js';
import {DefaultValueAccessorProvider} from '../value-accessors/providers/default-value-accessor-provider.js';
import {Form} from '../form.js';
import {FormGroupBuilderFactory} from './form-group-builder-factory.js';
import {ChainedValueAccessorProvider} from '../value-accessors/providers/chained-value-accessor-provider.js';
import {ComponentValueAccessorProvider} from '../value-accessors/providers/component-value-accessor-provider.js';


export type FormBuilderMembersFactory = (builder: FormGroupBuilder) => ReadonlyArray<FormMember<any>>;

export class FormBuilder
{
	constructor(
		private readonly valueAccessorProvider: ValueAccessorProvider,
	) {}

	public create<TItems>(host: ReactiveControllerHost, factory: FormBuilderMembersFactory, options: FormGroupBuilderOptions = {}): Form<TItems>
	{
		return new Form(host, new FormGroupBuilderFactory(host, this.valueAccessorProvider, options), factory);
	}
}

export const defaultFormBuilder = new FormBuilder(new ChainedValueAccessorProvider([
	new ComponentValueAccessorProvider(),
	new DefaultValueAccessorProvider(),
]));
