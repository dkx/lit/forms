import type {ReactiveControllerHost} from 'lit';

import type {ValueAccessorProvider} from '../value-accessors/providers/value-accessor-provider.js';
import type {FormParent} from '../form-parent.js';
import {FormGroupBuilder, FormGroupBuilderOptions} from './form-group-builder.js';


export class FormGroupBuilderFactory
{
	constructor(
		private readonly host: ReactiveControllerHost,
		private readonly valueAccessorProvider: ValueAccessorProvider,
		private readonly groupOptions: FormGroupBuilderOptions,
	) {}

	public create(parent: FormParent): FormGroupBuilder
	{
		return new FormGroupBuilder(this.host, this, this.valueAccessorProvider, parent, this.groupOptions);
	}
}
