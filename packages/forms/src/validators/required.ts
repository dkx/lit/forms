import type {FormControlValidator} from '../validation/form-control-validator.js';
import {FormControlError} from '../validation/form-control-error.js';


export function required(message: string): FormControlValidator<any>
{
	return control => {
		if (control.isEmpty()) {
			return new FormControlError(message);
		}

		return null;
	};
}
