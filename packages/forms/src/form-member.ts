import type {Observable} from 'rxjs';

import type {FormControl} from './form-control.js';


export interface FormMember<TValue>
{
	readonly name: string;

	value: TValue|null;

	readonly valueChanges: Observable<TValue>;

	readonly valid: boolean;

	/** @internal */
	destroy(): void;

	/** @internal */
	getControls(): ReadonlyArray<FormControl<any>>;
}
