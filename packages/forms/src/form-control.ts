import type {ReactiveControllerHost} from 'lit';
import type {DirectiveResult} from 'lit/directive';
import type {Observable} from 'rxjs';
import {Subject} from 'rxjs';

import type {FormControlValueAccessor} from './value-accessors/form-control-value-accessor.js';
import type {ValueAccessorProvider} from './value-accessors/providers/value-accessor-provider.js';
import type {FormControlValidator} from './validation/form-control-validator.js';
import type {AttachmentDirective} from './directives/attachment-directive.js';
import type {FormMember} from './form-member.js';
import type {FormParent} from './form-parent.js';
import {FormControlError} from './validation/form-control-error.js';
import {attachment} from './directives/attachment-directive.js';
import {ValueAccessorsRegistry} from './value-accessors/value-accessors-registry.js';


export declare interface FormControlOptions<TValue>
{
	validators?: ReadonlyArray<FormControlValidator<TValue>>,
	reportErrorsOnTouched?: boolean,
	reportErrorsOnChanged?: boolean,
}

export class FormControl<TValue> implements FormMember<TValue>
{
	private readonly _valueChanges: Subject<TValue> = new Subject<TValue>();

	private _touched: boolean = false;

	private _dirty: boolean = false;

	private _value: TValue|null;

	private _valid: boolean = true;

	private _error: FormControlError|null = null;

	private _errorReported: boolean = false;

	private _errorReportRequested: boolean = false;

	private readonly validators: ReadonlyArray<FormControlValidator<TValue>>;

	private readonly registry: ValueAccessorsRegistry<FormControlValueAccessor<TValue>> = new ValueAccessorsRegistry();

	private readonly reportErrorsOnTouched: boolean;

	private readonly reportErrorsOnChanged: boolean;

	constructor(
		private readonly host: ReactiveControllerHost,
		private readonly valueAccessorProvider: ValueAccessorProvider,
		private readonly parent: FormParent,
		public readonly name: string,
		private readonly defaultValue: TValue|null,
		options: FormControlOptions<TValue> = {}
	) {
		this._value = this.defaultValue;
		this.validators = options.validators ?? [];
		this.reportErrorsOnTouched = options.reportErrorsOnTouched ?? true;
		this.reportErrorsOnChanged = options.reportErrorsOnChanged ?? true;
	}

	public get value(): TValue|null
	{
		return this._value;
	}
	public set value(value: TValue|null)
	{
		this._value = value;
		this.registry.first?.writeValue(value);
		this._valueChanges.next(this._value);
	}

	public get valueChanges(): Observable<TValue>
	{
		return this._valueChanges;
	}

	public get touched(): boolean
	{
		return this._touched;
	}

	public get dirty(): boolean
	{
		return this._dirty;
	}

	public get valid(): boolean
	{
		return this._valid;
	}

	public get error(): FormControlError|null
	{
		return this._error;
	}

	public get errorReported(): boolean
	{
		return this._errorReported;
	}

	public get errorReportRequested(): boolean
	{
		return this._errorReportRequested;
	}

	public attach(): DirectiveResult<typeof AttachmentDirective>
	{
		return attachment(this);
	}

	public render(content: (control: FormControl<TValue>) => unknown): unknown
	{
		return content(this);
	}

	public validate(): boolean
	{
		return this.doValidate(true);
	}

	public reportValidity(): void
	{
		this._errorReportRequested = true;
		this._errorReported = false;

		const valueAccessor = this.registry.first;

		if (typeof valueAccessor !== 'undefined') {
			if (!this._valid && this._error === null) {
				// skip when native validity is false, show browser message
			} else {
				valueAccessor.setCustomValidity(this._error);
			}

			if (valueAccessor.reportValidity()) {
				this._errorReported = true;
				this._errorReportRequested = false;
			}
		}
	}

	public focus(): void
	{
		this.registry.first?.focus();
	}

	public isEmpty(): boolean
	{
		return this.registry.first?.isEmpty() ?? true;
	}

	public reset(): void
	{
		this.value = null;
	}

	/** @internal */
	public destroy(): void
	{
		this.registry.accessors.forEach((valueAccessor, el) => {
			this.disconnect(el);
		});

		this._valueChanges.complete();
	}

	/** @internal */
	public async connect(el: Element): Promise<void>
	{
		await this.host.updateComplete;

		const valueAccessor = this.valueAccessorProvider.create(el, this.registry);
		if (valueAccessor === null) {
			throw new Error(`FormControl: there is no known value accessor for element <${el.localName}>`);
		}

		const firstValueAccessor = this.registry.first;
		if (typeof firstValueAccessor !== 'undefined') {
			if (firstValueAccessor.constructor !== valueAccessor.constructor) {
				throw new Error(`Element <${el.localName}> can not be attached to value accessor ${firstValueAccessor.constructor.name}`);
			}

			if (!firstValueAccessor.supportsMultipleElements) {
				throw new Error(`Value accessor ${firstValueAccessor.constructor.name} does not support attaching to multiple elements`);
			}
		}

		this.registry.add(el, valueAccessor);

		valueAccessor.writeValue(this._value);
		this._value = valueAccessor.value;		// read real value when null was default
		this.doValidate(false);
		this._valueChanges.next(this._value);

		valueAccessor.registerOnTouched(() => {
			this._touched = true;
			this.doValidate(this.reportErrorsOnTouched);
		});

		valueAccessor.registerOnChanged(() => {
			const value = valueAccessor.value;
			this._value = value;
			this._dirty = value !== this.defaultValue;
			this.doValidate(this.reportErrorsOnChanged);
			this._valueChanges.next(this._value);
		});
	}

	/** @internal */
	public disconnect(el: Element): void
	{
		const valueAccessor = this.registry.get(el);
		if (typeof valueAccessor !== 'undefined') {
			valueAccessor.disconnect();
			this.registry.remove(el);
		}
	}

	/** @internal */
	public getControls(): ReadonlyArray<FormControl<any>>
	{
		return [this];
	}

	private doValidate(reportError: boolean): boolean
	{
		const valueAccessor = this.registry.first;

		if (typeof valueAccessor === 'undefined') {
			return this.setValidationState(reportError, true, null);
		}

		if (!valueAccessor.valid) {
			return this.setValidationState(reportError, false, null);
		}

		for (let validator of this.validators) {
			const error = validator(this);
			if (error !== null) {
				return this.setValidationState(reportError, false, error);
			}
		}

		return this.setValidationState(reportError, true, null);
	}

	private setValidationState(reportError: boolean, valid: boolean, error: FormControlError|null): boolean
	{
		const changed = this._valid !== valid || errorEquals(this._error, error);

		this._valid = valid;
		this._error = error;

		if (changed && (this._valid || (reportError && (this._touched || this._dirty)))) {
			this.reportValidity();
		}

		return this._valid;
	}
}

function errorEquals(previous: FormControlError|null, next: FormControlError|null): boolean
{
	if (previous !== null && next !== null) {
		return previous.message === next.message;
	}

	if (previous === null && next !== null) {
		return false;
	}

	if (previous !== null && next === null) {
		return false;
	}

	return true;
}
