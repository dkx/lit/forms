import type {ElementPart, PartInfo} from 'lit/directive.js';
import {PartType} from 'lit/directive.js';
import {AsyncDirective, directive} from 'lit/async-directive.js';
import {nothing} from 'lit/html.js';

import type {FormControl} from '../form-control.js';


export class AttachmentDirective<TValue> extends AsyncDirective
{
	private readonly el: Element;

	private control: FormControl<TValue>|null = null;

	constructor(partInfo: PartInfo)
	{
		super(partInfo);

		if (partInfo.type !== PartType.ELEMENT) {
			throw new Error('Form control can be attached only to an element');
		}

		this.el = (partInfo as ElementPart).element;
	}

	public render(control: FormControl<TValue>)
	{
		if (this.control !== null) {
			if (this.control === control) {
				return nothing;
			}

			this.control.disconnect(this.el);
			this.control = null;
		}

		this.control = control;
		this.control.connect(this.el).then(() => {
			// do nothing
		});

		return nothing;
	}

	protected disconnected(): void
	{
		this.control?.disconnect(this.el);
	}

	protected reconnected(): void
	{
		this.control?.connect(this.el).then(() => {
			// do nothing
		});
	}
}

export const attachment = directive(AttachmentDirective);
