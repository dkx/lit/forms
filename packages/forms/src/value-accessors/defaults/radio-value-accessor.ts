import type {FormControlValueAccessor} from '../form-control-value-accessor.js';
import type {ValueAccessorsRegistry} from '../value-accessors-registry.js';
import type {FormControlChanged, FormControlTouched} from '../form-control-value-accessor.js';
import type {FormControlError} from '../../validation/form-control-error.js';


export class RadioValueAccessor implements FormControlValueAccessor<string>
{
	public readonly supportsMultipleElements = true;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: HTMLInputElement,
		private readonly registry: ValueAccessorsRegistry<RadioValueAccessor>,
	) {
		this.changeListener = () => {
			if (this.el.checked) {
				this.changed();
			}
		};

		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return this.el.validity.valid;
	}

	public get value(): string|null
	{
		if (this.el.checked) {
			return this.el.value;
		}

		let hasEmpty = false;

		for (let [el, va] of this.registry.accessors) {
			const radio = el as HTMLInputElement;
			if (radio.checked) {
				return radio.value;
			}

			if (radio.value === '') {
				hasEmpty = true;
			}
		}

		return hasEmpty ? '' : null;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		this.el.reportValidity();
		return true;
	}

	public writeValue(value: string|null): void
	{
		if (value === null) {
			for (let [el, va] of this.registry.accessors) {
				va.el.checked = false;
			}

			return;
		}

		for (let [el, va] of this.registry.accessors) {
			if (va.el.value === value) {
				va.el.checked = true;
				return;
			}
		}
	}

	public setDisabledState(state: boolean): void
	{
		for (let [el, va] of this.registry.accessors) {
			va.el.disabled = state;
		}
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		for (let [el, va] of this.registry.accessors) {
			if (va.el.checked) {
				return false;
			}
		}

		return true;
	}
}
