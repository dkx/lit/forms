import type {FormControlValueAccessor} from '../form-control-value-accessor.js';
import type {FormControlChanged, FormControlTouched} from '../form-control-value-accessor.js';
import type {FormControlError} from '../../validation/form-control-error.js';


export class SelectMultipleValueAccessor implements FormControlValueAccessor<ReadonlyArray<string>>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly changeListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: HTMLSelectElement,
	)
	{
		this.changeListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('change', this.changeListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return this.el.validity.valid;
	}

	public get value(): ReadonlyArray<string>|null
	{
		const values: Array<string> = [];
		for (let i = 0; i < this.el.selectedOptions.length; i++) {
			values.push(this.el.selectedOptions[i].value);
		}

		return values;
	}

	public disconnect()
	{
		this.el.removeEventListener('change', this.changeListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		this.el.reportValidity();
		return true;
	}

	public writeValue(value: ReadonlyArray<string>|null): void
	{
		if (value === null) {
			for (let i = 0; i < this.el.options.length; i++) {
				this.el.options[i].selected = false;
			}

			return;
		}

		for (let i = 0; i < this.el.options.length; i++) {
			this.el.options[i].selected = value.indexOf(this.el.options[i].value) >= 0;
		}
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return this.el.selectedOptions.length === 0;
	}
}
