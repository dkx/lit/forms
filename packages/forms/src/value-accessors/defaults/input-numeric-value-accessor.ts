import type {FormControlError} from '../../validation/form-control-error.js';
import type {FormControlChanged, FormControlTouched, FormControlValueAccessor} from '../form-control-value-accessor.js';


export class InputNumericValueAccessor implements FormControlValueAccessor<number>
{
	public readonly supportsMultipleElements = false;

	private touched: FormControlTouched = () => {};

	private changed: FormControlChanged = () => {};

	private readonly inputListener: () => void;

	private readonly blurListener: () => void;

	constructor(
		private readonly el: HTMLInputElement
	) {
		this.inputListener = () => this.changed();
		this.blurListener = () => this.touched();

		this.el.addEventListener('input', this.inputListener);
		this.el.addEventListener('blur', this.blurListener);
	}

	public get valid(): boolean
	{
		return this.el.validity.valid;
	}

	public get value(): number|null
	{
		return this.el.valueAsNumber;
	}

	public disconnect()
	{
		this.el.removeEventListener('input', this.inputListener);
		this.el.removeEventListener('blur', this.blurListener);
	}

	public registerOnTouched(fn: FormControlTouched): void
	{
		this.touched = fn;
	}

	public registerOnChanged(fn: FormControlChanged): void
	{
		this.changed = fn;
	}

	public setCustomValidity(error: FormControlError|null): void
	{
		this.el.setCustomValidity(error?.message ?? '');
	}

	public reportValidity(): boolean
	{
		this.el.reportValidity();
		return true;
	}

	public writeValue(value: number|null): void
	{
		if (value === null) {
			this.el.value = '';
			return;
		}

		if (typeof value === 'string') {
			this.el.value = value;
		} else {
			this.el.valueAsNumber = value;
		}
	}

	public setDisabledState(state: boolean): void
	{
		this.el.disabled = state;
	}

	public focus(): void
	{
		this.el.focus();
	}

	public isEmpty(): boolean
	{
		return this.el.value.length === 0;
	}
}
