import type {FormControlValueAccessor} from './form-control-value-accessor.js';


export class ValueAccessorsRegistry<TAccessor extends FormControlValueAccessor<any>>
{
	public readonly accessors: Map<Element, TAccessor> = new Map();

	private _first: TAccessor|undefined;

	public get first(): TAccessor|undefined
	{
		return this._first;
	}

	public get(el: Element): TAccessor|undefined
	{
		return this.accessors.get(el);
	}

	public add(el: Element, accessor: TAccessor): void
	{
		if (typeof this._first === 'undefined') {
			this._first = accessor;
		}

		this.accessors.set(el, accessor);
	}

	public remove(el: Element): void
	{
		const accessor = this.accessors.get(el);
		this.accessors.delete(el);

		if (accessor === this._first) {
			this._first = undefined;

			for (let [element, va] of this.accessors) {
				this._first = va;
				break;
			}
		}

	}
}
