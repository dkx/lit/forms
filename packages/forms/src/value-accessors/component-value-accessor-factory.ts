import type {FormControlValueAccessor} from './form-control-value-accessor.js';
import type {ValueAccessorsRegistry} from './value-accessors-registry.js';


export interface ComponentValueAccessorFactory<TValue>
{
	createValueAccessor(registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<TValue>;
}
