import type {FormControlValueAccessor} from '../form-control-value-accessor.js';
import type {ValueAccessorsRegistry} from '../value-accessors-registry.js';


export interface ValueAccessorProvider
{
	create(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null;
}
