import type {ValueAccessorProvider} from './value-accessor-provider.js';
import type {FormControlValueAccessor} from '../form-control-value-accessor.js';
import type {ValueAccessorsRegistry} from '../value-accessors-registry.js';


export class ChainedValueAccessorProvider implements ValueAccessorProvider
{
	constructor(
		private readonly providers: ReadonlyArray<ValueAccessorProvider>,
	) {}

	public create(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null
	{
		for (let provider of this.providers) {
			const accessor = provider.create(el, registry);
			if (accessor !== null) {
				return accessor;
			}
		}

		return null;
	}
}
