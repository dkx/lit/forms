import type {ValueAccessorProvider} from './value-accessor-provider.js';
import type {FormControlValueAccessor} from '../form-control-value-accessor.js';
import type {ValueAccessorsRegistry} from '../value-accessors-registry.js';
import {InputValueAccessor} from '../defaults/input-value-accessor.js';
import {CheckboxValueAccessor} from '../defaults/checkbox-value-accessor.js';
import {RadioValueAccessor} from '../defaults/radio-value-accessor.js';
import {SelectValueAccessor} from '../defaults/select-value-accessor.js';
import {SelectMultipleValueAccessor} from '../defaults/select-multiple-value-accessor.js';
import {TextareaValueAccessor} from '../defaults/textarea-value-accessor.js';
import {InputNumericValueAccessor} from '../defaults/input-numeric-value-accessor.js';


export class DefaultValueAccessorProvider implements ValueAccessorProvider
{
	public create(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null
	{
		if (el instanceof HTMLInputElement) {
			if (el.type === 'checkbox') {
				return new CheckboxValueAccessor(el);
			}

			if (el.type === 'radio') {
				return new RadioValueAccessor(el, registry);
			}

			if (el.type === 'number' || el.type === 'range') {
				return new InputNumericValueAccessor(el);
			}

			return new InputValueAccessor(el);
		}

		if (el instanceof HTMLSelectElement) {
			if (el.multiple) {
				return new SelectMultipleValueAccessor(el);
			}

			return new SelectValueAccessor(el);
		}

		if (el instanceof HTMLTextAreaElement) {
			return new TextareaValueAccessor(el);
		}

		return null;
	}
}
