import {ValueAccessorProvider} from './value-accessor-provider.js';
import {FormControlValueAccessor} from '../form-control-value-accessor.js';
import {ValueAccessorsRegistry} from '../value-accessors-registry.js';


const METHOD_NAME = 'createValueAccessor';

export class ComponentValueAccessorProvider implements ValueAccessorProvider
{
	public create(el: Element, registry: ValueAccessorsRegistry<any>): FormControlValueAccessor<any>|null
	{
		if (typeof el[METHOD_NAME] === 'function') {
			return el[METHOD_NAME](registry);
		}

		return null;
	}
}
