import type {FormControlError} from '../validation/form-control-error.js';


export type FormControlTouched = () => void;
export type FormControlChanged = () => void;

export interface FormControlValueAccessor<TValue>
{
	readonly supportsMultipleElements: boolean;

	readonly valid: boolean;

	readonly value: TValue|null;

	disconnect(): void;

	registerOnTouched(fn: FormControlTouched): void;

	registerOnChanged(fn: FormControlChanged): void;

	setCustomValidity(error: FormControlError|null): void;

	reportValidity(): boolean;

	writeValue(value: TValue|null): void;

	setDisabledState(state: boolean): void;

	focus(): void;

	isEmpty(): boolean;
}
