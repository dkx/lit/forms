import type {ReactiveController, ReactiveControllerHost} from 'lit';

import type {FormBuilderMembersFactory} from './builders/form-builder.js';
import type {FormGroupBuilderFactory} from './builders/form-group-builder-factory.js';
import {FormBaseGroup} from './form-base-group.js';


export class Form<TItems> extends FormBaseGroup<TItems> implements ReactiveController
{
	constructor(
		private readonly host: ReactiveControllerHost,
		groupBuilderFactory: FormGroupBuilderFactory,
		factory: FormBuilderMembersFactory,
	) {
		super('@root', null, groupBuilderFactory, factory);

		this.valueChanges.subscribe(() => {
			this.host.requestUpdate();
		});

		this.host.addController(this);
	}

	public render(content: (form: Form<TItems>) => unknown): unknown
	{
		return content(this);
	}

	public validate(stopOnFirst: boolean = false): boolean
	{
		if (this.valid) {
			return true;
		}

		let focused = false;

		for (let control of this.getControls()) {
			if (!control.valid) {
				control.reportValidity();
				if (!focused) {
					focused = true;
					control.focus();
				}

				if (stopOnFirst) {
					break;
				}
			}
		}

		this.host.requestUpdate();

		return false;
	}

	/** @internal */
	public hostUpdated(): void
	{
	}

	/** @internal */
	public destroy(): void
	{
		this.host.removeController(this);
		super.destroy();
	}
}
