import type {DirectiveResult} from 'lit/directive';
import type {Observable, Subscription} from 'rxjs';
import {Subject} from 'rxjs';

import type {AttachmentDirective} from './directives/attachment-directive.js';
import type {FormBuilderMembersFactory} from './builders/form-builder.js';
import type {FormParent} from './form-parent.js';
import type {FormGroupBuilderFactory} from './builders/form-group-builder-factory.js';
import type {FormMember} from './form-member.js';
import {attachment} from './directives/attachment-directive.js';
import {FormControl} from './form-control.js';


export type FormControls<T> = {[P in keyof T]-?: FormMember<any>};

export abstract class FormBaseGroup<TItems> implements FormParent
{
	public readonly controls: FormControls<TItems>;

	private readonly _valueChanges: Subject<TItems> = new Subject<TItems>();

	private subscriptions: ReadonlyArray<Subscription>;

	private _valid: boolean = true;

	private readonly _value: TItems;

	private patchingInProgress: boolean = false;

	protected constructor(
		public readonly name: string,
		private readonly parent: FormParent|null,
		groupBuilderFactory: FormGroupBuilderFactory,
		factory: FormBuilderMembersFactory,
	)
	{
		const members = factory(groupBuilderFactory.create(this));
		const controls = {};
		const value = {};
		const subscriptions: Array<Subscription> = [];

		for (let member of members) {
			controls[member.name] = member;
			value[member.name] = member.value;

			subscriptions.push(member.valueChanges.subscribe(value => {
				this._value[member.name] = value;

				if (this.patchingInProgress) {
					return;
				}

				this._valid = this.checkValidState();
				this._valueChanges.next(this._value);
			}));
		}

		this.controls = controls as FormControls<TItems>;
		this._value = value as TItems;
		this.subscriptions = subscriptions;
	}

	public get value(): TItems|null
	{
		return this._value;
	}
	public set value(value: TItems|null)
	{
		if (value === null) {
			return;
		}

		this.patchValues(value);
	}

	public get valueChanges(): Observable<TItems>
	{
		return this._valueChanges;
	}

	public get valid(): boolean
	{
		return this._valid;
	}

	public patchValues(values: Partial<TItems>): void
	{
		this.patchingInProgress = true;

		for (let name in values) {
			if (values.hasOwnProperty(name) && typeof this.controls[name] !== 'undefined') {
				this.controls[name].value = values[name];
			}
		}

		this._valid = this.checkValidState();
		this.patchingInProgress = false;
		this._valueChanges.next(this._value);
	}

	public reset(): void
	{
		for (let control of this.getControls()) {
			control.reset();
		}
	}

	public attach(name: string): DirectiveResult<typeof AttachmentDirective>
	{
		if (typeof this.controls[name] === 'undefined') {
			throw new Error(`Can not attach form control "${name}" because there is non defined with this name`);
		}

		const control = this.controls[name];

		if (!(control instanceof FormControl)) {
			throw new Error(`Can not attach form member "${name}" because it is not a form control`);
		}

		return attachment(control);
	}

	/** @internal */
	public getControls(): ReadonlyArray<FormControl<any>>
	{
		let controls = [];
		for (let name in this.controls) {
			if (this.controls.hasOwnProperty(name)) {
				controls = controls.concat(this.controls[name].getControls());
			}
		}

		return controls;
	}

	/** @internal */
	public destroy(): void
	{
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}

		this.subscriptions = [];
		this._valueChanges.complete();
	}

	private checkValidState(): boolean
	{
		for (let name in this.controls) {
			if (this.controls.hasOwnProperty(name)) {
				if (!this.controls[name].valid) {
					return false;
				}
			}
		}

		return true;
	}
}
